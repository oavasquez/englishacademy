const express = require('express');
const bodyParser = require('body-parser');
const router = express.Router();
const config = require('../config/config');
const verificarToken = require('../config/verificarToken');

const app = express();
app.set('claveSecreta', config.claveSecreta);

const jwt = require('jsonwebtoken');

const alumno = require('../models/alumno.model');
const dashboard = require('../models/dashboard.model');
const matricula = require('../models/matricula.model');
const tipoIncidencia = require('../models/tipoIncidente.model');
const tutor = require('../models/tutor.model');
const inicidencia = require('../models/incidente.model');
const historial = require('../models/historial.model');
const traslado = require('../models/traslado.model');
const seccion = require('../models/seccion.model');




router.use(bodyParser.urlencoded({ extended: false }));
var urlencodedParser = bodyParser.urlencoded({ extended: false });
var jsonParser = bodyParser.json();


exports.mostrarMensaje = function (req, res) {

    res.status(200).json({ mensaje: "Bienvenido" });

};


//**********************************************Alumno REST********************************************************
// Handle create on POST.
exports.alumno_detalles_post = function (req, res) {
    console.log("en la funcion de crear alumno");
    alumno.mostrarAlumnos(req, function (row) {

        if (row.length > 0) {

            res.status(200).json(JSON.parse(JSON.stringify(row[0])));

        } else {
            res.status(200).json({ mensaje: 0 });
        }

    });
};

// Handle create on POST.
exports.alumno_detalles_resumen_post = function (req, res) {
    console.log("en la funcion de crear alumno");
    alumno.mostrarAlumnoResumen(req, function (row) {

        if (row.length > 0) {

            res.status(200).json(JSON.parse(JSON.stringify(row[0])));

        } else {
            res.status(200).json({ mensaje: 0 });
        }

    });
};


// Handle create on POST.
exports.alumno_crear_post = function (req, res) {
    console.log("en la funcion de crear alumno");
    alumno.crearAlumno(req, function (row) {

        if (row > 0) {

            res.status(200).json({ mensaje: 1 });

        } else {
            res.status(200).json({ mensaje: 0 });
        }

    });

};

// Handle update on POST.
exports.alumno_actualizar_post = function (req, res) {
    console.log("en la funcion de actualizar alumno");
    alumno.actualizarAlumno(req, function (row) {

        if (row.affectedRows > 0) {

            res.status(200).json({ mensaje: 1 });

        } else {
            res.status(200).json({ mensaje: 0 });
        }

    });
};
//**********************************************Inicidencia REST********************************************************
// Handle create on POST.
exports.inicidencia_detalles_post = function (req, res) {
    console.log("en la funcion de crear inicidencia");
    inicidencia.mostrarIncidente(req, function (row) {

        if (row.length > 0) {

            res.status(200).json(JSON.parse(JSON.stringify(row[0])));

        } else {
            res.status(200).json({ mensaje: 0 });
        }

    });
};

// Handle create on POST.
exports.inicidencia_crear_post = function (req, res) {
    console.log("en la funcion de crear inicidencia");
    inicidencia.crearIncidente(req, function (row) {

        if (row.affectedRows > 0) {

            res.status(200).json({ mensaje: 1 });

        } else {
            res.status(200).json({ mensaje: 0 });
        }

    });

};

// Handle update on POST.
exports.inicidencia_actualizar_post = function (req, res) {
    console.log("en la funcion de actualizar inicidencia");
    inicidencia.actualizarIncidente(req, function (row) {

        if (row.affectedRows > 0) {

            res.status(200).json({ mensaje: 1 });

        } else {
            res.status(200).json({ mensaje: 0 });
        }

    });
};

//**********************************************Matricula REST********************************************************
// Handle create on POST.
exports.matricula_detalles_post = function (req, res) {
    console.log("en la funcion de crear matricula");
    matricula.mostrarMatricula(req, function (row) {

        if (row.length > 0) {

            res.status(200).json(JSON.parse(JSON.stringify(row[0])));

        } else {
            res.status(200).json({ mensaje: 0 });
        }

    });
};

// Handle create on POST.
exports.matricula_crear_post = function (req, res) {
    console.log("en la funcion de crear matricula");
    matricula.crearMatricula(req, function (row) {

        if (row.affectedRows > 0) {

            res.status(200).json({ mensaje: 1 });

        } else {
            res.status(200).json({ mensaje: 0 });
        }

    });

};






// Handle create on POST.
exports.matricula_seccion_crear_post = function (req, res) {
    console.log("en la funcion de crear matricula seccion");
    matricula.crearMatriculaSeccion(req, function (row) {

        if (row.affectedRows > 0) {

            res.status(200).json({ mensaje: 1 });

        } else {
            res.status(200).json({ mensaje: 0 });
        }

    });

};

// Handle update on POST.
exports.matricula_actualizar_post = function (req, res) {
    console.log("en la funcion de actualizar matricula");
    matricula.actualizarMatricula(req, function (row) {

        if (row.affectedRows > 0) {

            res.status(200).json({ mensaje: 1 });

        } else {
            res.status(200).json({ mensaje: 0 });
        }

    });
};
//**********************************************TipoIncidencia REST********************************************************
// Handle create on POST.
exports.tipoIncidencia_detalles_post = function (req, res) {
    console.log("en la funcion de crear tipoIncidencia");
    tipoIncidencia.mostrarTipoIncidente(req, function (row) {

        if (row.length > 0) {

            res.status(200).json(JSON.parse(JSON.stringify(row[0])));

        } else {
            res.status(200).json({ mensaje: 0 });
        }

    });
};



// Handle create on POST.
exports.tipoIncidencia_crear_post = function (req, res) {
    console.log("en la funcion de crear tipoIncidencia");
    tipoIncidencia.crearTipoIncidente(req, function (row) {

        if (row.affectedRows > 0) {

            res.status(200).json({ mensaje: 1 });

        } else {
            res.status(200).json({ mensaje: 0 });
        }

    });

};

// Handle update on POST.
exports.tipoIncidencia_actualizar_post = function (req, res) {
    console.log("en la funcion de actualizar tipoIncidencia");
    tipoIncidencia.actualizarTipoIncidente(req, function (row) {

        if (row.affectedRows > 0) {

            res.status(200).json({ mensaje: 1 });

        } else {
            res.status(200).json({ mensaje: 0 });
        }

    });
};

//**********************************************Tutor REST********************************************************
// Handle create on POST.
exports.tutor_detalles_post = function (req, res) {
    console.log("en la funcion de crear tutor");
    tutor.mostrarTutores(req, function (row) {

        if (row.length > 0) {

            res.status(200).json(JSON.parse(JSON.stringify(row[0])));

        } else {
            res.status(200).json({ mensaje: 0 });
        }

    });
};



// Handle create on POST.
exports.tutor_crear_post = function (req, res) {
    console.log("en la funcion de crear tutor");
    tutor.crearTutor(req, function (row) {

        if (row.affectedRows > 0) {

            res.status(200).json({ mensaje: 1 });

        } else {
            res.status(200).json({ mensaje: 0 });
        }

    });

};

// Handle update on POST.
exports.tutor_actualizar_post = function (req, res) {
    console.log("en la funcion de actualizar tutor");
    tutor.actualizarTutor(req, function (row) {

        if (row.affectedRows > 0) {

            res.status(200).json({ mensaje: 1 });

        } else {
            res.status(200).json({ mensaje: 0 });
        }

    });
};

//**********************************************Traslado REST********************************************************
// Handle create on POST.
exports.traslado_detalles_post = function (req, res) {
    console.log("en la funcion de crear traslado");
    traslado.mostrarTraslado(req, function (row) {

        if (row.length > 0) {

            res.status(200).json(JSON.parse(JSON.stringify(row[0])));

        } else {
            res.status(200).json({ mensaje: 0 });
        }

    });
};



// Handle create on POST.
exports.traslado_crear_post = function (req, res) {
    console.log("en la funcion de crear traslado");
    traslado.crearTraslado(req, function (row) {

        if (row > 0) {

            res.status(200).json({ mensaje: 1 });

        } else {
            res.status(200).json({ mensaje: 0 });
        }

    });

};

// Handle update on POST.
exports.traslado_actualizar_post = function (req, res) {
    console.log("en la funcion de actualizar traslado");
    traslado.actualizarTraslado(req, function (row) {

        if (row.affectedRows > 0) {

            res.status(200).json({ mensaje: 1 });

        } else {
            res.status(200).json({ mensaje: 0 });
        }

    });
};

//**********************************************REST Historial********************************************************************
// Handle create on POST.
exports.historial_detalles_post = function (req, res) {
    console.log("en la funcion de mostrar historial");
    historial.mostrarHistorial(req, function (row) {

        if (row.length > 0) {

            res.status(200).json(JSON.parse(JSON.stringify(row[0])));

        } else {
            res.status(200).json({ mensaje: 0 });
        }

    });
};

///****************************************************Secciones****************************************/




exports.secciones_disponibles_detalles_post = function (req, res) {
    console.log("en la funcion de mostrar historial");
    seccion.mostrarSeccionesDisponibles(req, function (row) {

        if (row.length > 0) {

            res.status(200).json(JSON.parse(JSON.stringify(row[0])));

        } else {
            res.status(200).json({ mensaje: 0 });
        }

    });
};


//**********************************************Dashboard MatriculaRegistro********************************************************
// Handle create on POST.
exports.dashboard_matricula_registro_detalles_post = function (req, res) {
    console.log("en la funcion para obtener todos los datos de dashboard de matricula y registro");
    dashboard.mostrarDashboardMatriculaRegistro(req, function (row) {

        if (row.length > 0) {

            res.status(200).json(JSON.parse(JSON.stringify(row[0])));

        } else {
            res.status(200).json({ mensaje: 0 });
        }

    });
};