const express = require('express');
const bodyParser = require('body-parser');
const router = express.Router();
const config = require('../config/config');
const verificarToken = require('../config/verificarToken');

//models import
const persona = require('../models/persona.model');
const usuario = require('../models/user.model');
const rol = require('../models/tipoUsuario.model');
const curso = require('../models/curso.model');
const asignatura = require('../models/asignatura.model');
const modalidad = require('../models/modalidad.model');
const periodo = require('../models/periodo.model');
const jornada = require('../models/jornada.model');
const lectivo = require('../models/lectivos.model')
const dashboard = require('../models/dashboard.model');

const app = express();
app.set('claveSecreta', config.claveSecreta);

const jwt = require('jsonwebtoken');



router.use(bodyParser.urlencoded({ extended: false }));
var urlencodedParser = bodyParser.urlencoded({ extended: false });
var jsonParser = bodyParser.json();


exports.mostrarMensaje = function (req, res) {

    res.status(200).json({ mensaje: "Bienvenido" });

};

//**********************************************usuario REST********************************************************
// Handle create on POST.
exports.usuario_detalles_post = function (req, res) {
    console.log("en la funcion de crear usuario");
    usuario.mostrarUsuarios(req, function (row) {

        if (row.length > 0) {

            res.status(200).json(JSON.parse(JSON.stringify(row[0])));

        } else {
            res.status(200).json({ mensaje: 0 });
        }

    });
};

// Handle Author create on POST.
exports.usuario_detalles_get = function (req, res) {
    res.send('NOT IMPLEMENTED: user show GET');
};

// Handle create on POST.
exports.usuario_crear_post = function (req, res) {
    console.log("en la funcion de crear usuario");
    usuario.crearUsuario(req, function (row) {

        if (row.affectedRows > 0) {

            res.status(200).json({ mensaje: 1 });

        } else {
            res.status(200).json({ mensaje: 0 });
        }

    });

};
exports.usuario_crear_get = function (req, res) {
    res.send('NOT IMPLEMENTED: user create GET');
};

// Display delete form on GET.
exports.usuario_eliminar_get = function (req, res) {
    res.send('NOT IMPLEMENTED: user delete GET');
};

// Handle delete on POST.
exports.usuario_eliminar_post = function (req, res) {
    res.send('NOT IMPLEMENTED: user delete POST');
};

// Display update form on GET.
exports.usuario_actualizar_get = function (req, res) {
    res.send('NOT IMPLEMENTED: user update GET');
};

// Handle update on POST.
exports.usuario_actualizar_post = function (req, res) {
    console.log("en la funcion de actualizar usuario");
    usuario.actualizarUsuario(req, function (row) {

        if (row.affectedRows > 0) {

            res.status(200).json({ mensaje: 1 });

        } else {
            res.status(200).json({ mensaje: 0 });
        }

    });
};



//******************************************************Rol REST****************************************************+
//obtiene la informacion de los roles que existen por metodo post
exports.rol_detalles_post = function (req, res) {
    console.log("En la funcion de mostrar los roles");
    rol.mostrarRol(req, function (row) {

        if (row.length > 0) {

            res.status(200).json(JSON.parse(JSON.stringify(row[0])));

        } else {
            res.status(200).json({ mensaje: 0 });
        }

    });
};

//obtiene la informacion de los roles que existen por metodo get
exports.rol_detalles_get = function (req, res) {
    res.send('NOT IMPLEMENTED: rol show POST');
};

//crea un nuevo rol por metodo post
exports.rol_crear_post = function (req, res) {
    rol.crearRol(req, function (row) {

        if (row.affectedRows > 0) {

            res.status(200).json({ mensaje: 1 });

        } else {
            res.status(200).json({ mensaje: 0 });
        }

    });

};
//crea un nuevo rol por metodo get
exports.rol_crear_get = function (req, res) {
    res.send('NOT IMPLEMENTED: rol create GET');
};
//elimina un rol existente por metodo post
exports.rol_eliminar_post = function (req, res) {
    res.send('NOT IMPLEMENTED: rol delete POST');
};
//elimina un rol existente por metodo get
exports.rol_eliminar_get = function (req, res) {
    res.send('NOT IMPLEMENTED: rol delete GET');
};
//elimina un rol existente por metodo post
exports.rol_actualizar_post = function (req, res) {

    rol.actualizarRol(req, function (row) {

        if (row.affectedRows > 0) {

            res.status(200).json({ mensaje: 1 });

        } else {
            res.status(200).json({ mensaje: 0 });
        }

    });
};
//actualiza un rol existente por metodo get
exports.rol_actualizar_get = function (req, res) {
    res.send('NOT IMPLEMENTED: rol update GET');
};



//*************************************************curso REST*********************************************************


// Handle create on POST.
exports.curso_detalles_post = function (req, res) {
    curso.mostrarCurso(req, function (row) {

        if (row.length > 0) {

            res.status(200).json(JSON.parse(JSON.stringify(row[0])));

        } else {
            res.status(200).json({ mensaje: 0 });
        }

    });
};

// Handle Author create on POST.
exports.curso_detalles_get = function (req, res) {
    res.send('NOT IMPLEMENTED: Author create GET');
};

// Handle create on POST.
exports.curso_crear_post = function (req, res) {
    curso.crearCurso(req, function (row) {
        console.log(row);

        if (row > 0) {

            res.status(200).json({ mensaje: 1 });

        } else {
            res.status(200).json({ mensaje: 0 });
        }

    });
};
exports.curso_crear_get = function (req, res) {
    res.send('NOT IMPLEMENTED: Author create GET');
};

// Display delete form on GET.
exports.curso_eliminar_get = function (req, res) {
    res.send('NOT IMPLEMENTED: Author delete GET');
};

// Handle delete on POST.
exports.curso_eliminar_post = function (req, res) {
    res.send('NOT IMPLEMENTED: Author delete POST');
};


// Handle update on POST.
exports.curso_actualizar_post = function (req, res) {
    curso.actualizarCurso(req, function (row) {

        if (row.affectedRows > 0) {

            res.status(200).json({ mensaje: 1 });

        } else {
            res.status(200).json({ mensaje: 0 });
        }

    });
};

// Display update form on GET.
exports.curso_actualizar_get = function (req, res) {
    res.send('NOT IMPLEMENTED: Author update GET');
};


//***********************************************lectivo REST********************************************************

// Handle create on POST.
exports.lectivo_detalles_post = function (req, res) {
    lectivo.mostrarLectivo(req, function (row) {

        if (row.length > 0) {

            res.status(200).json(JSON.parse(JSON.stringify(row[0])));

        } else {
            res.status(200).json({ mensaje: 0 });
        }

    });
};

// Handle Author create on POST.
exports.lectivo_detalles_get = function (req, res) {
    res.send('NOT IMPLEMENTED: Author create POST');
};

// Handle create on POST.
exports.lectivo_crear_post = function (req, res) {
    lectivo.crearLectivo(req, function (row) {

        if (row.affectedRows > 0) {

            res.status(200).json({ mensaje: 1 });

        } else {
            res.status(200).json({ mensaje: 0 });
        }

    });
};
exports.lectivo_crear_get = function (req, res) {
    res.send('NOT IMPLEMENTED: Author create POST');
};

// Display delete form on GET.
exports.lectivo_eliminar_get = function (req, res) {
    res.send('NOT IMPLEMENTED: Author delete GET');
};

// Handle delete on POST.
exports.lectivo_eliminar_post = function (req, res) {
    res.send('NOT IMPLEMENTED: Author delete POST');
};

// Display update form on GET.
exports.lectivo_actualizar_get = function (req, res) {
    res.send('NOT IMPLEMENTED: Author update GET');
};

// Handle update on POST.
exports.lectivo_actualizar_post = function (req, res) {
    lectivo.actualizarLectivo(req, function (row) {

        if (row.affectedRows > 0) {

            res.status(200).json({ mensaje: 1 });

        } else {
            res.status(200).json({ mensaje: 0 });
        }

    });
};

//***************************************************Asignatura REST*************************************************
// Handle create on POST.
exports.asignatura_detalles_post = function (req, res) {
    asignatura.mostrarAsignatura(req, function (row) {

        if (row.length > 0) {

            res.status(200).json(JSON.parse(JSON.stringify(row[0])));

        } else {
            res.status(200).json({ mensaje: 0 });
        }

    });
};
//lista de todas las asignaturas
exports.asignatura_list_detalles_post = function (req, res) {
    asignatura.mostrarListaAsignatura(req, function (row) {

        if (row.length > 0) {

            res.status(200).json(JSON.parse(JSON.stringify(row[0])));

        } else {
            res.status(200).json({ mensaje: 0 });
        }

    });
};

// Handle Asignatura create on POST.
exports.asignatura_detalles_get = function (req, res) {
    res.send('NOT IMPLEMENTED: Asignatura create POST');
};

// Handle create on POST.
exports.asignatura_crear_post = function (req, res) {
    asignatura.crearAsignatura(req, function (row) {

        if (row.affectedRows > 0) {

            res.status(200).json({ mensaje: 1 });

        } else {
            res.status(200).json({ mensaje: 0 });
        }

    });
};
exports.asignatura_crear_get = function (req, res) {
    res.send('NOT IMPLEMENTED: Asignatura create POST');
};


// Handle delete on POST.
exports.asignatura_eliminar_post = function (req, res) {
    res.send('NOT IMPLEMENTED: Asignatura delete POST');
};

// Display delete form on GET.
exports.asignatura_eliminar_get = function (req, res) {
    res.send('NOT IMPLEMENTED: Asignatura delete GET');
};

// Handle update on POST.
exports.asignatura_actualizar_post = function (req, res) {
    asignatura.actualizarAsignatura(req, function (row) {

        if (row.affectedRows > 0) {

            res.status(200).json({ mensaje: 1 });

        } else {
            res.status(200).json({ mensaje: 0 });
        }

    });
};
// Display update form on GET.
exports.asignatura_actualizar_get = function (req, res) {
    res.send('NOT IMPLEMENTED: Asignatura update GET');
};


//***************************************************Modalidad REST*************************************************
// Handle create on POST.
exports.modalidad_detalles_post = function (req, res) {
    modalidad.mostrarModalidad(req, function (row) {

        if (row.length > 0) {

            res.status(200).json(JSON.parse(JSON.stringify(row[0])));

        } else {
            res.status(200).json({ mensaje: 0 });
        }

    });
};

// Handle Asignatura create on POST.
exports.modalidad_detalles_get = function (req, res) {
    res.send('NOT IMPLEMENTED: Modalidad create POST');
};

// Handle create on POST.
exports.modalidad_crear_post = function (req, res) {
    modalidad.crearModalidad(req, function (row) {

        if (row.affectedRows > 0) {

            res.status(200).json({ mensaje: 1 });

        } else {
            res.status(200).json({ mensaje: 0 });
        }

    });
};
exports.modalidad_crear_get = function (req, res) {
    res.send('NOT IMPLEMENTED: Modalidad create POST');
};


// Handle delete on POST.
exports.modalidad_eliminar_post = function (req, res) {
    res.send('NOT IMPLEMENTED: Modalidad delete POST');
};

// Display delete form on GET.
exports.modalidad_eliminar_get = function (req, res) {
    res.send('NOT IMPLEMENTED: Modalidad delete GET');
};

// Handle update on POST.
exports.modalidad_actualizar_post = function (req, res) {
    modalidad.actualizarModalidad(req, function (row) {

        if (row.affectedRows > 0) {

            res.status(200).json({ mensaje: 1 });

        } else {
            res.status(200).json({ mensaje: 0 });
        }

    });
};
// Display update form on GET.
exports.modalidad_actualizar_get = function (req, res) {
    res.send('NOT IMPLEMENTED: Modalidad update GET');
};

//***************************************************Periodo REST*************************************************
// Handle create on POST.
exports.periodo_detalles_post = function (req, res) {
    periodo.mostrarPeriodo(req, function (row) {

        if (row.length > 0) {

            res.status(200).json(JSON.parse(JSON.stringify(row[0])));

        } else {
            res.status(200).json({ mensaje: 0 });
        }

    });
};



// Handle create on POST.
exports.periodo_crear_post = function (req, res) {
    periodo.crearPeriodo(req, function (row) {

        if (row.affectedRows > 0) {

            res.status(200).json({ mensaje: 1 });

        } else {
            res.status(200).json({ mensaje: 0 });
        }

    });
};

// Handle delete on POST.
exports.periodo_eliminar_post = function (req, res) {
    res.send('NOT IMPLEMENTED: Periodo delete POST');
};

// Handle update on POST.
exports.periodo_actualizar_post = function (req, res) {
    periodo.actualizarPeriodo(req, function (row) {

        if (row.affectedRows > 0) {

            res.status(200).json({ mensaje: 1 });

        } else {
            res.status(200).json({ mensaje: 0 });
        }

    });
};

//*****************************************Jornada REST****************************************************/
// Handle create on POST.
exports.jornada_detalles_post = function (req, res) {
    jornada.mostrarJornada(req, function (row) {

        if (row.length > 0) {

            res.status(200).json(JSON.parse(JSON.stringify(row[0])));

        } else {
            res.status(200).json({ mensaje: 0 });
        }

    });
};



// Handle create on POST.
exports.jornada_crear_post = function (req, res) {
    jornada.crearJornada(req, function (row) {

        if (row.affectedRows > 0) {

            res.status(200).json({ mensaje: 1 });

        } else {
            res.status(200).json({ mensaje: 0 });
        }

    });
};

// Handle delete on POST.
exports.jornada_eliminar_post = function (req, res) {
    res.send('NOT IMPLEMENTED: Jornada delete POST');
};

// Handle update on POST.
exports.jornada_actualizar_post = function (req, res) {
    jornada.actualizarJornada(req, function (row) {

        if (row.affectedRows > 0) {

            res.status(200).json({ mensaje: 1 });

        } else {
            res.status(200).json({ mensaje: 0 });
        }

    });
};


//***************************************************other functions*************************************************
//obtiene todos los nombres de persona de la base junto con su id de personas  
exports.get_nombres_personas_post = function (req, res) {

    console.log("estor en get_all_personas_get ");
    persona.getNombresPersonas(req, function (row) {
        if (row.length > 0) {

            res.status(200).json(JSON.parse(JSON.stringify(row[0])));

        } else {
            res.status(200).json({ mensaje: 0 });
        }

    });

};
//obtiene todos los nombres de los usuarios de la base junto con su id de personas  
exports.get_nombres_usuarios_post = function (req, res) {

    console.log("estor en get_nombre_usuario_post ");
    usuario.obtenerNombresUsuarios(req, function (row) {
        if (row.length > 0) {

            res.status(200).json(JSON.parse(JSON.stringify(row[0])));

        } else {
            res.status(200).json({ mensaje: 0 });
        }

    });

};

// Asignar una asignatura a un curso
exports.asignatura_cursos_post = function (req, res) {
    asignatura.asignarAsignaturaCurso(req, function (row) {

        if (row.affectedRows > 0) {

            res.status(200).json({ mensaje: 1 });

        } else {
            res.status(200).json({ mensaje: 0 });
        }

    });
};

// Asignar una asignatura a un curso
exports.asignatura_cursos_actualizar_post = function (req, res) {
    asignatura.actualizarAsignaturaCurso(req, function (row) {

        if (row.affectedRows > 0) {

            res.status(200).json({ mensaje: 1 });

        } else {
            res.status(200).json({ mensaje: 0 });
        }

    });
};

//**********************************************Dashboard Administracion********************************************************
// Handle create on POST.
exports.dashboard_administracion_detalles_post = function (req, res) {
    console.log("en la funcion para obtener todos los datos de dashboard de administracion");
    dashboard.mostrarDashboardAdministracion(req, function (row) {

        if (row.length > 0) {

            res.status(200).json(JSON.parse(JSON.stringify(row[0])));

        } else {
            res.status(200).json({ mensaje: 0 });
        }

    });
};
