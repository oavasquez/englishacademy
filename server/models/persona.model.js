var dataBaseConnection = require('../config/dataBaseConnection');


exports.getDetallesPersona = function (req, done) {
    dataBaseConnection.get().query('call sp_get_persona(?)', [req.body.codigoPersona], function (err, rows) {
        if (err) return done(err)
        done(rows);
    })
}

exports.getNombresPersonas = function (req, done) {
    dataBaseConnection.get().query("call sp_get_nombre_personas()", function (err, rows) {
        if (err) return done(err)
        done(rows);
    })
}
