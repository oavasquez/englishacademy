var dataBaseConnection = require('../config/dataBaseConnection');

//REST Incidente 
//Mostrar Incidente 
exports.mostrarIncidente = function (req, done) {
    dataBaseConnection.get().query('call sp_get_incidencias(?,?)', [req.body.codPersona, req.body.parametro], function (err, rows) {
        if (err) return done(err)
        done(rows);
    })
}
//crear Incidente 
exports.crearIncidente = function (req, done) {

    console.log( req.body)
    dataBaseConnection.get().query('call sp_insert_incidente(?,?,?,?,?,?,?,?)', [
        req.body.tituloIncidencia,
        req.body.descripcion,
        req.body.codTipoIncidencia,
        req.body.codAlumno,
        req.body.codDocente,
        req.body.fechaIncidencia,
        req.body.codDocente,
        req.body.codAlumno

    ], function (err, rows) {
        if (err) return done(err)
        console.log(err)
        done(rows);
    })
}
//Actualizar Incidente 
exports.actualizarIncidente = function (req, done) {
    dataBaseConnection.get().query('call sp_update_incidencia(?,?,?,?,?,?,?,?,?)', [
        req.body.codIncidencia,
        req.body.tituloIncidencia,
        req.body.descripcion,
        req.body.codTipoIncidencia,
        req.body.codAlumno,
        req.body.codDocente,
        req.body.fecha,
        req.body.codPersonaReporta,
        req.body.codPersonaReportada
    ], function (err, rows) {
        if (err) return done(err)
        done(rows);
    })
}
//Eliminar Incidente 
exports.eiminarIncidente = function (req, done) {
    dataBaseConnection.get().query('', [], function (err, rows) {
        if (err) return done(err)
        done(rows);
    })
}