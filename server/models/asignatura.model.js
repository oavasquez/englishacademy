var dataBaseConnection = require('../config/dataBaseConnection');

//REST Asignatura
//Mostrar Asignatura
exports.mostrarAsignatura = function (req, done) {
    dataBaseConnection.get().query('call sp_get_asignaturas_curso(?,?)', [req.body.codCurso, req.body.habilitado], function (err, rows) {
        if (err) return done(err)
        done(rows);
    });
}

exports.mostrarListaAsignatura = function (req, done) {
    dataBaseConnection.get().query('CALL sp_get_asignaturas(?)', [req.body.codAsignatura], function (err, rows) {
        if (err) return done(err)
        done(rows);
    });
}
//agregar una asignatura a un curso
exports.asignarAsignaturaCurso = function (req, done) {
    console.log(req.body);
    dataBaseConnection.get().query('call sp_insert_asignatura_de_curso(?,?,?)', [req.body.codCurso, req.body.codAsignatura, req.body.diasAsignatura], function (err, rows) {
        if (err) return done(err)
        done(rows);
    })
}

//Actualizar Asignatura
exports.actualizarAsignatura = function (req, done) {

    dataBaseConnection.get().query('call sp_update_Asignatura(?,?)', [req.body.codAsignatura, req.body.asignatura], function (err, rows) {
        if (err) return done(err)
        done(rows);
    })

}


//Actualizar Asignatura por un curso
exports.actualizarAsignaturaCurso = function (req, done) {
    console.log(req.body);

    dataBaseConnection.get().query('call sp_update_asignatura_x_curso(?,?,?,?,?,?)', [req.body.codCurso, req.body.codAsignatura, req.body.asignatura, req.body.dias, req.body.estadoAsignatura, req.body.nuevoCodCurso], function (err, rows) {
        if (err) return done(err)
        done(rows);
    })
}


//Eliminar Asignatura
exports.eiminarAsignatura = function (req, done) {
    dataBaseConnection.get().query('call (?,?,?,?)', [req.body.nombreUsuario], function (err, rows) {
        if (err) return done(err)
        done(rows);
    })
}

//other function 


//crear Asignatura
exports.crearAsignatura = function (req, done) {
    dataBaseConnection.get().query('call sp_insert_asignatura(?,?)', [req.body.asignatura, req.body.estadoAsignatura], function (err, rows) {
        if (err) return done(err)
        done(rows);
    })
}