const express = require('express');
const bodyParser = require('body-parser');
const router = express.Router();


var matriculaRegistro_server_controller = require('../controllers/matriculaRegistro.server.controller');


const verificarToken = require('../config/verificarToken');


router.use(bodyParser.urlencoded({ extended: false }));

var urlencodedParser = bodyParser.urlencoded({ extended: false });

var jsonParser = bodyParser.json();


router.get('/', (req, res) => {

    //console.log(JSON.stringify(respuesta));
    res.status(200).json({
        mensaje: 'raiz matriculaRegistro'
    });
});

//REST alumno
//Mostrar alumnos
router.post('/mostrarAlumnos', jsonParser, matriculaRegistro_server_controller.alumno_detalles_post);
router.post('/mostrarAlumnoResumen', jsonParser, matriculaRegistro_server_controller.alumno_detalles_resumen_post);
//Crear alumno
router.post('/crearAlumno', jsonParser, matriculaRegistro_server_controller.alumno_crear_post);
//Actualizar Alumno
router.post('/actualizarAlumno', jsonParser, matriculaRegistro_server_controller.alumno_actualizar_post);


//REST incidencia
//Mostrar incidencias
router.post('/mostrarIncidentes', jsonParser, matriculaRegistro_server_controller.inicidencia_detalles_post);
//Crear incidencia
router.post('/crearIncidencia', jsonParser, matriculaRegistro_server_controller.inicidencia_crear_post);
//Actualizar Incidencia
router.post('/actualizarIncidencia', jsonParser, matriculaRegistro_server_controller.inicidencia_actualizar_post);

//REST traslado
//Mostrar traslados
router.post('/mostrarTraslados', jsonParser, matriculaRegistro_server_controller.traslado_detalles_post);
//Crear traslado
router.post('/crearTraslado', jsonParser, matriculaRegistro_server_controller.traslado_crear_post);
//Actualizar Traslado
router.post('/actualizarTraslado', jsonParser, matriculaRegistro_server_controller.traslado_actualizar_post);


//REST historial
//Mostrar historial
router.post('/mostrarHistorial', jsonParser, matriculaRegistro_server_controller.historial_detalles_post);
//Crear incidencia



//REST incidencia
//Mostrar incidencias
router.post('/mostrarTipoIncidencia', jsonParser, matriculaRegistro_server_controller.tipoIncidencia_detalles_post);
//Crear incidencia
router.post('/crearTipoIncidencia', jsonParser, matriculaRegistro_server_controller.tipoIncidencia_crear_post);
//Actualizar TipoIncidencia
router.post('/actualizarTipoIncidencia', jsonParser, matriculaRegistro_server_controller.tipoIncidencia_actualizar_post);


//REST matricula
//Mostrar matriculas
router.post('/mostrarMatriculas', jsonParser, matriculaRegistro_server_controller.matricula_detalles_post);
//Crear matricula
router.post('/crearMatricula', jsonParser, matriculaRegistro_server_controller.matricula_crear_post);
//Actualizar Matricula
router.post('/crearMatriculaSeccion', jsonParser, matriculaRegistro_server_controller.matricula_crear_post);
//Actualizar Matricula
router.post('/actualizarMatriculaNotas', jsonParser, matriculaRegistro_server_controller.matricula_actualizar_post);


//REST tutor
//Mostrar tutores
router.post('/mostrarTutores', jsonParser, matriculaRegistro_server_controller.tutor_detalles_post);
//Crear tutor
router.post('/crearTutor', jsonParser, matriculaRegistro_server_controller.tutor_crear_post);
//Actualizar Tutor
router.post('/actualizarTutor', jsonParser, matriculaRegistro_server_controller.tutor_actualizar_post);


//secciones
router.post('/mostrarSeccionesDisponibles', jsonParser, matriculaRegistro_server_controller.secciones_disponibles_detalles_post);



//obtiene todos los datos para mostrar en los dashboard de matricula y registro
router.post('/mostrarDashboardMatriculaRegistro', jsonParser, matriculaRegistro_server_controller.dashboard_matricula_registro_detalles_post);


module.exports = router;