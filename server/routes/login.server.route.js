const express = require('express');
const bodyParser = require('body-parser');
const router = express.Router();

var login_server_controller = require('../controllers/login.server.controller');

const verificarToken = require('../config/verificarToken');



router.use(bodyParser.urlencoded({ extended: false }))

var urlencodedParser = bodyParser.urlencoded({ extended: false })

var jsonParser = bodyParser.json()




router.get('/', (req, res) => {

  //console.log(JSON.stringify(respuesta));
  res.status(200).json({
    mensaje: 'raiz login'
  });
});

router.post('/iniciarSesion', jsonParser, login_server_controller.verificarCredenciales);
router.post('/mostarMensaje', jsonParser, login_server_controller.mostrarMensaje);







module.exports = router;

