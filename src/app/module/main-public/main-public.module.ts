import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MainPublicRoutingModule } from './main-public-routing.module';
import { LandingContanierComponent } from './component/landing-contanier/landing-contanier.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { LandingCoverContainerComponent } from './component/landing-cover-container/landing-cover-container.component';
import { BlogContainerComponent } from './component/blog-container/blog-container.component';


@NgModule({
  declarations: [LandingContanierComponent, LandingCoverContainerComponent, BlogContainerComponent],
  imports: [
    CommonModule,
    MainPublicRoutingModule,
    NgbModule
  ]
})
export class MainPublicModule { }
