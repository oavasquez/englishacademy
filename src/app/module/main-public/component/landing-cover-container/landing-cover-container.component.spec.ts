import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LandingCoverContainerComponent } from './landing-cover-container.component';

describe('LandingCoverContainerComponent', () => {
  let component: LandingCoverContainerComponent;
  let fixture: ComponentFixture<LandingCoverContainerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LandingCoverContainerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LandingCoverContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
