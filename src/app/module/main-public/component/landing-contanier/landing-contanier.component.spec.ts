import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LandingContanierComponent } from './landing-contanier.component';

describe('LandingContanierComponent', () => {
  let component: LandingContanierComponent;
  let fixture: ComponentFixture<LandingContanierComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LandingContanierComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LandingContanierComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
