import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BlogContainerComponent } from './component/blog-container/blog-container.component';
import { LandingContanierComponent } from './component/landing-contanier/landing-contanier.component';
import { LandingCoverContainerComponent } from './component/landing-cover-container/landing-cover-container.component';

const routes: Routes = [
  {
    path: '',
    component: LandingContanierComponent
  },

  {
    path: 'cover',
    component: LandingCoverContainerComponent
  },
  
  {
    path: 'blog',
    component: BlogContainerComponent
  },
  

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainPublicRoutingModule { }
