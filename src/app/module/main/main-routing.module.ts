import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainContainerComponent } from './component/main-container/main-container.component';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./main-admin/main-admin.module').then(m => m.MainAdminModule)
  },
  {
    path: 'student',
    loadChildren: () => import('./main-student/main-student.module').then(m => m.MainStudentModule)
  },
  {
    path: 'teacher',
    loadChildren: () => import('./main-teacher/main-teacher.module').then(m => m.MainTeacherModule)
  },
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainRoutingModule { }
