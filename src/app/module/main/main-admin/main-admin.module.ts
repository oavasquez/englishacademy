import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MainAdminRoutingModule } from './main-admin-routing.module';
import { MainContainerComponent } from './component/main-container/main-container.component';


@NgModule({
  declarations: [MainContainerComponent],
  imports: [
    CommonModule,
    MainAdminRoutingModule
  ]
})
export class MainAdminModule { }
