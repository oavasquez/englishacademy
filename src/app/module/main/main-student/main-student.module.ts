import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MainStudentRoutingModule } from './main-student-routing.module';
import { MainContainerComponent } from './component/main-container/main-container.component';
import { CursosContainerComponent } from './component/cursos-container/cursos-container.component';


@NgModule({
  declarations: [MainContainerComponent, CursosContainerComponent],
  imports: [
    CommonModule,
    MainStudentRoutingModule
  ]
})
export class MainStudentModule { }
