import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MainTeacherRoutingModule } from './main-teacher-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    MainTeacherRoutingModule
  ]
})
export class MainTeacherModule { }
