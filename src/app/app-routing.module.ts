import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [

  
    {
      path: '',
      loadChildren: () => import('./module/login/login.module').then(m => m.LoginModule)
    },

    {
      path: 'main',
      loadChildren: () => import('./module/main/main.module').then(m => m.MainModule)
    },

    {
      path: 'public',
      loadChildren: () => import('./module/main-public/main-public.module').then(m => m.MainPublicModule)
    }
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
